# coding: utf-8

from bs4 import BeautifulSoup
import requests
import string
import cx_Oracle

# extract games and short names to hit the AFL website
query = """
select season_id, group_round_no, home_sms_short_name, away_sms_short_name
from afl_fixture_v
where competition_level_id=14
and season_id>=2015
and match_status_id=6
"""
con = cx_Oracle.connect('swiz_readonly/read1swiz@swizprd')
cur = con.cursor()
cur.execute(query)
res = cur.fetchall()
cur.close()

# now run through each game
corpus = ""
for season_id, group_round_no, home_squad, away_squad in res[:]:
	# this is the structure of afl.com.au match reports
	url = "http://www.afl.com.au/match-centre/%i/%i/%s-v-%s"%(season_id, group_round_no, home_squad.lower(), away_squad.lower())
	try:
		# hit URL
		r  = requests.get(url)
		data = r.text
		soup = BeautifulSoup(data, 'lxml')

		# extract the match report
		entry = soup.findAll('div',{'class':"story-content"})
		# replace some unicode characters. 
		# There must be a better way to do this
		text = entry[0].text.encode('utf-8').replace("\'", "'").replace('\xc2','').replace('\xa0','').replace("\xe2\x80\x99","'", ).replace("\xe2\x80\x9c", "").replace("\xe2\x80\x98","'").replace("\xe2\x80\x93", "-")

		# get rid of paragraphs that include certain words. 
		# There doesn't seem to be a consistent structure for this, so it's a bit ad-hoc
		filter_words = ['pic.twitter', '@', 't.co', 'MEDICAL ROOM', 'GOALS', 'BEST', 'INJURIES', 'Official crowd:', 'Reports:', 'Umpires:', "Watch:", "Picture:", 'UP NEXT', 'NEXT UP', 'UP', 'NEW', 'Five talking points:', 'SUBSTITUTES']
		filter_func = lambda x: max([i in x for i in filter_words])==False
		text_filtered = filter(filter_func, text.split('\n'))
		text_reconstructed = " ".join([i for i in text_filtered if i!=""])
		
		# get rid of double spaces
		while "  " in text_reconstructed:
			text_reconstructed = text_reconstructed.replace("  ", " ")

		# sometimes there are missing spaces, depending on how the text has been reformatted
		for p in ')!?.],}':
			for letter in string.letters:
				text_reconstructed = text_reconstructed.replace("%s%s"%(p, letter), "%s %s"%(p, letter))

		# starts of paragraphs start with all-caps words
		# summary scores are also presented in all-caps
		# un-capitlise all these words, unless they are a ground name. 
		# think I've got all of them below, but I might have forgotten some
		text_deconstructed = text_reconstructed.split()
		for i, word in enumerate(text_deconstructed):
				if word==word.upper() and word not in ['MCG','SCG','TIO','WACA']:
					text_deconstructed[i] = word.capitalize()
		
		# rejoin
		text_reconstructed = " ".join(text_deconstructed)

		# add to the corpus
		corpus += text_reconstructed+". "
	except:
		print url
		print 'Error!'
		pass

f = open("AFL Corpus.txt", "w")
f.write(corpus)
f.close()