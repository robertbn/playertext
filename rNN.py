# coding: utf-8

import numpy as np
import theano
import theano.tensor as T
import lasagne
import urllib2
import time
import string

#try:
#    in_text = urllib2.urlopen('https://s3.amazonaws.com/text-datasets/nietzsche.txt').read()
#    in_text = in_text.decode("utf-8-sig").encode("utf-8")
#except Exception as e:
#    print("Please verify the location of the input file/URL.")
#    print("A sample txt file can be downloaded from https://s3.amazonaws.com/text-datasets/nietzsche.txt")
#    raise IOError('Unable to Read Text')
# Clean the data a bit
#in_text = in_text.replace('\n',' ')
#punctuation = """'"#$%/()*&,-/:;@"""
#in_text = in_text.translate(None, punctuation)

f = open("C:/Users/admin/Desktop/Rob/bitbucket-repos/playertext/AFL corpus.txt", "r")
in_text = f.read()
f.close()

# This phrase will be used as seed to generate text.
generation_phrase = "Greater Western Sydney" 	

# Extract unique characters and make some useful dictionaries
#in_text = in_text.split(' ')
chars = list(set(in_text))									# unique characters
data_size, vocab_size = len(in_text), len(chars)			# total length of data, and total number of unique characters
char_to_ix = {char:ix for ix, char in enumerate(chars)}		# index and character dictionaries
ix_to_char = {ix:char for ix, char in enumerate(chars)}

# Random number seed for reproducibility
lasagne.random.set_rng(np.random.RandomState(1))

# Sequence Length
seq_length = 20
shift_size = 5			# if the memory is too limited to look at every reduntant length-20 string, change the shift size
n_hidden = 512			# Number of units in the two hidden (LSTM) layers
learning_rate = .01		# Optimization learning rate
grad_clip = 100			# All gradients above this will be clipped
epochs = 100			# Number of epochs to train the net
batch_size = 128		# Batch Size

# arrange training data (use all of it to start)
p = 0
train_size = int((data_size-seq_length)/shift_size)
x_train = np.zeros((train_size, seq_length, vocab_size))
y_train = np.zeros(train_size)

for n in xrange(train_size):
	for i in xrange(seq_length):
		x_train[n, i, char_to_ix[in_text[p+n*shift_size+i]]] = 1.
		y_train[n] = char_to_ix[in_text[p+n*shift_size+seq_length]]

# build the recurrent network
print("Building network ...")

# First, we build the network, starting with an input layer
# Recurrent layers expect input of shape
# (batch size, seq_length, num_features)
l_in = lasagne.layers.InputLayer(shape=(None, None, vocab_size))

# We now build the LSTM layer which takes l_in as the input layer
# We clip the gradients at grad_clip to prevent the problem of exploding gradients. 
l_forward_1 = lasagne.layers.LSTMLayer(incoming=l_in, num_units=n_hidden, grad_clipping=grad_clip, nonlinearity=lasagne.nonlinearities.tanh)
l_forward_2 = lasagne.layers.LSTMLayer(incoming=l_forward_1, num_units=n_hidden, grad_clipping=grad_clip, nonlinearity=lasagne.nonlinearities.tanh, only_return_final=True)

# The output of l_forward_2 of shape (batch_size, n_hidden) is then passed through the softmax nonlinearity to 
# create probability distribution of the prediction
# The output of this stage is (batch_size, vocab_size)
l_out = lasagne.layers.DenseLayer(incoming=l_forward_2, num_units=vocab_size, W=lasagne.init.Normal(), nonlinearity=lasagne.nonlinearities.softmax)

# Theano tensor for the targets
target_values = T.ivector('target_output')

# lasagne.layers.get_output produces a variable for the output of the net
network_output = lasagne.layers.get_output(l_out)

# The loss function is calculated as the mean of the (categorical) cross-entropy between the prediction and target.
cost = T.nnet.categorical_crossentropy(network_output, target_values).mean()

# Retrieve all parameters from the network
all_params = lasagne.layers.get_all_params(l_out, trainable=True)

# Compute AdaGrad updates for training
print("Computing updates ...")
updates = lasagne.updates.adagrad(cost, all_params, learning_rate)

# Theano functions for training and computing cost
print("Compiling functions ...")
train = theano.function([l_in.input_var, target_values], cost, updates=updates, allow_input_downcast=True)
compute_cost = theano.function([l_in.input_var, target_values], cost, allow_input_downcast=True)

# In order to generate text from the network, we need the probability distribution of the next character given
# the state of the network and the input (a seed).
# In order to produce the probability distribution of the prediction, we compile a function called probs. 
probs = theano.function([l_in.input_var], network_output, allow_input_downcast=True)

# The next function generates text given a phrase of length at least seq_length.
# The phrase is set using the variable generation_phrase.
# The optional input "N" is used to set the number of characters of text to predict. 
def try_it_out(N=200):
	'''
	This function uses the user-provided string "generation_phrase" and current state of the RNN generate text.
	The function works in three steps:
	1. It converts the string set in "generation_phrase" (which must be over seq_length characters long) 
	   to encoded format. We use the gen_data function for this. By providing the string and asking for a single batch,
	   we are converting the first seq_length characters into encoded form. 
	2. We then use the LSTM to predict the next character and store it in a (dynamic) list sample_ix. This is done by using the 'probs'
	   function which was compiled above. Simply put, given the output, we compute the probabilities of the target and pick the one 
	   with the highest predicted probability. 
	3. Once this character has been predicted, we construct a new sequence using all but first characters of the 
	   provided string and the predicted character. This sequence is then used to generate yet another character.
	   This process continues for "N" characters. 
	To make this clear, let us again look at a concrete example. 
	Assume that seq_length = 5 and generation_phrase = "The quick brown fox jumps". 
	We initially encode the first 5 characters ('T','h','e',' ','q'). The next character is then predicted (as explained in step 2). 
	Assume that this character was 'J'. We then construct a new sequence using the last 4 (=seq_length-1) characters of the previous
	sequence ('h','e',' ','q') , and the predicted letter 'J'. This new sequence is then used to compute the next character and 
	the process continues.
	'''

	assert(len(generation_phrase)>=seq_length)
	sample_ix = []
	
	x = np.zeros((1,seq_length,vocab_size))
	for i in range(seq_length):
		x[0,i,char_to_ix[generation_phrase[len(generation_phrase)-seq_length+i]]] = 1.

	for i in range(N):
		# Pick the character that got assigned the highest probability
		ix = np.argmax(probs(x).ravel())
		sample_ix.append(ix)
		x[:,0:seq_length-1,:] = x[:,1:,:]
		x[:,seq_length-1,:] = 0
		x[0,seq_length-1,sample_ix[-1]] = 1. 

	pred_sentence = generation_phrase + ''.join(ix_to_char[ix] for ix in sample_ix)    
	print pred_sentence
					
# Run the training function in mini-batches.
print "Training ..."
print "Seed used for text generation is: " + generation_phrase
n_examples = x_train.shape[0]				# number of training examples we have
n_batches = n_examples / batch_size			# number of mini batches we will run (depends on batch size)

# loop over a certain number of epochs
cost_history = []
for epoch in xrange(epochs):
	# shuffle training data every epoch
	# use an index array so we can shuffle the independent and dependent variables the same
	s = np.arange(n_examples)	
	np.random.shuffle(s)
	# loop over each batch
	etick = time.time()	# time it
	batch_cost_history = []
	for batch in xrange(n_batches):
		if batch%100==0:
			print "%i/%i"%(batch+1, n_batches)
		x_batch = x_train[s[batch*batch_size: (batch+1)*batch_size]]	# split up the examples into this batch
		y_batch = y_train[s[batch*batch_size: (batch+1)*batch_size]]	# split up the targets into this batch
		this_cost = train(x_batch, y_batch) 							# This is where the model gets updated. This returns the predictions (using the dropout layer)
		batch_cost_history.append(this_cost)							# update cost/loss history
	# assess the validation loss for the model. Print it out if it's better.
	epoch_cost = np.mean(batch_cost_history)							# update total batch_cost_history
	cost_history.append(epoch_cost)
	etock = time.time()
	print 'Epoch %d/%d, train cost: %0.5f. Elapsed time: %0.2fs.'%(epoch+1, epochs, epoch_cost, etock-etick)
	try_it_out() # Generate text using the p^th character as the start. 